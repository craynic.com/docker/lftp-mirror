FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# renovate: datasource=repology depName=alpine_3_21/lftp depType=dependencies versioning=loose
ARG LFTP_VERSION="4.9.2-r7"

RUN apk add --no-cache \
    bash=~5 \
    lftp="${LFTP_VERSION}" \
    openssh=~9

COPY files/ /

ENV LFTP_SERVER_PUBLIC_KEYS="" \
    LFTP_CLIENT_PRIVATE_KEY="" \
    LFTP_CLIENT_HOSTNAME="" \
    LFTP_CLIENT_USERNAME="" \
    LFTP_CLIENT_LOCAL_FOLDER="" \
    LFTP_CLIENT_REMOTE_FOLDER=""

CMD ["/bin/bash"]
