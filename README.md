# LFTP-mirror command

Usage:

```text
lftp-mirror
  -h <hostname>       Hostname to connect to; defaults to environment variable "LFTP_CLIENT_HOSTNAME"
  -u <username>       SFTP username to use; defaults to environment variable "LFTP_CLIENT_USERNAME"
  -k <private key>    Path to a private SSH key to use; defaults to environment variable "LFTP_CLIENT_PRIVATE_KEY"
  -l <local folder>   Local folder to synchronize; defaults to environment variable "LFTP_CLIENT_LOCAL_FOLDER"
  -r <remote folder>  Remote folder to synchronize; defaults to environment variable "LFTP_CLIENT_REMOTE_FOLDER"
```
